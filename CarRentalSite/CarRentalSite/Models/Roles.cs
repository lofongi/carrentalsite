﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class Roles
    {
        public Roles()
        {
            UserInRoles = new HashSet<UserInRoles>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; }

        public ICollection<UserInRoles> UserInRoles { get; set; }
    }
}

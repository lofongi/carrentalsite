﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class Cars
    {
        public Cars()
        {
            CarFeedBacks = new HashSet<CarFeedBacks>();
            Imgs = new HashSet<Imgs>();
            Orders = new HashSet<Orders>();
        }

        public int CarId { get; set; }
        public int? UserId { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }

        public Users User { get; set; }
        public ICollection<CarFeedBacks> CarFeedBacks { get; set; }
        public ICollection<Imgs> Imgs { get; set; }
        public ICollection<Orders> Orders { get; set; }
    }
}

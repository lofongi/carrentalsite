﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class Users
    {
        public Users()
        {
            CarFeedBacks = new HashSet<CarFeedBacks>();
            Cars = new HashSet<Cars>();
            Imgs = new HashSet<Imgs>();
            Orders = new HashSet<Orders>();
            UserInRoles = new HashSet<UserInRoles>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public int? Age { get; set; }

        public ICollection<CarFeedBacks> CarFeedBacks { get; set; }
        public ICollection<Cars> Cars { get; set; }
        public ICollection<Imgs> Imgs { get; set; }
        public ICollection<Orders> Orders { get; set; }
        public ICollection<UserInRoles> UserInRoles { get; set; }
    }
}

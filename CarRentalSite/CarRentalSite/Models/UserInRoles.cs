﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class UserInRoles
    {
        public int UserInRoleId { get; set; }
        public int? UserId { get; set; }
        public int? RoleId { get; set; }

        public Roles Role { get; set; }
        public Users User { get; set; }
    }
}

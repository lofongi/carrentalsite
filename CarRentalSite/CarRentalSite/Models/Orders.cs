﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class Orders
    {
        public int OrderId { get; set; }
        public int? CarId { get; set; }
        public int? UserId { get; set; }
        public DateTime? Period { get; set; }

        public Cars Car { get; set; }
        public Users User { get; set; }
    }
}

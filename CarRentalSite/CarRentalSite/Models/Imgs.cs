﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class Imgs
    {
        public int ImgId { get; set; }
        public int? CarId { get; set; }
        public int? UserId { get; set; }
        public byte[] Img { get; set; }

        public Cars Car { get; set; }
        public Users User { get; set; }
    }
}

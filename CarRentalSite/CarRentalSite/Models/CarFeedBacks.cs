﻿using System;
using System.Collections.Generic;

namespace CarRentalSite.Models
{
    public partial class CarFeedBacks
    {
        public int CarFeedBackId { get; set; }
        public int? CarId { get; set; }
        public int? UserId { get; set; }
        public string Comment { get; set; }
        public int? Rating { get; set; }

        public Cars Car { get; set; }
        public Users User { get; set; }
    }
}

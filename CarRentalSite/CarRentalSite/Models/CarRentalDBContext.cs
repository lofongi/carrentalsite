﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CarRentalSite.Models
{
    public partial class CarRentalDBContext : DbContext
    {
        public CarRentalDBContext()
        {
        }

        public CarRentalDBContext(DbContextOptions<CarRentalDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CarFeedBacks> CarFeedBacks { get; set; }
        public virtual DbSet<Cars> Cars { get; set; }
        public virtual DbSet<Imgs> Imgs { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<UserInRoles> UserInRoles { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\sqlexpress;Database=CarRentalDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CarFeedBacks>(entity =>
            {
                entity.HasKey(e => e.CarFeedBackId);

                entity.Property(e => e.CarFeedBackId)
                    .HasColumnName("CarFeedBackID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CarId).HasColumnName("CarID");

                entity.Property(e => e.Comment)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.CarFeedBacks)
                    .HasForeignKey(d => d.CarId)
                    .HasConstraintName("FK__CarFeedBa__CarID__1B0907CE");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.CarFeedBacks)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__CarFeedBa__UserI__1BFD2C07");
            });

            modelBuilder.Entity<Cars>(entity =>
            {
                entity.HasKey(e => e.CarId);

                entity.Property(e => e.CarId)
                    .HasColumnName("CarID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Engine)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Cars__UserID__1273C1CD");
            });

            modelBuilder.Entity<Imgs>(entity =>
            {
                entity.HasKey(e => e.ImgId);

                entity.Property(e => e.ImgId)
                    .HasColumnName("ImgID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CarId).HasColumnName("CarID");

                entity.Property(e => e.Img).HasMaxLength(1);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.Imgs)
                    .HasForeignKey(d => d.CarId)
                    .HasConstraintName("FK__Imgs__CarID__22AA2996");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Imgs)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Imgs__UserID__239E4DCF");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.Property(e => e.OrderId)
                    .HasColumnName("OrderID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CarId).HasColumnName("CarID");

                entity.Property(e => e.Period).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CarId)
                    .HasConstraintName("FK__Orders__CarID__1ED998B2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Orders__UserID__1FCDBCEB");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.Property(e => e.RoleId)
                    .HasColumnName("RoleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserInRoles>(entity =>
            {
                entity.HasKey(e => e.UserInRoleId);

                entity.Property(e => e.UserInRoleId)
                    .HasColumnName("UserInRoleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserInRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__UserInRol__RoleI__182C9B23");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserInRoles)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__UserInRol__UserI__173876EA");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SurName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
